const React = require('react');
const ReactDOM = require('react-dom');
const _ = require('lodash');

var AptList = require('./aptList');
var AddAppointment = require('./AddAppointment');
var SearchAppointments = require('./searchAppointments');

class MainInterface extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      myAppointments: [],
      aptBodyVisible: false,
      orderBy: 'petName',
      orderDir: 'asc',
      queryText: ''
    };
    this.deleteMessage = this.deleteMessage.bind(this);
    this.toggleAddDisplay = this.toggleAddDisplay.bind(this);
    this.addItem = this.addItem.bind(this);
    this.reOrder = this.reOrder.bind(this);
    this.searchApts = this.searchApts.bind(this);
  }

  componentDidMount() {
    this.serverRequest = $.get('./js/data.json', result => {
      let tempApts = result;
      this.setState({
        myAppointments: tempApts
      });
    });
  }

  componentWillUnmount() {
    this.serverRequest.abort();
  }

  deleteMessage(item) {
    let allApts = this.state.myAppointments;
    let newApts = _.without(allApts, item);
    this.setState({
      myAppointments: newApts
    });
  }

  toggleAddDisplay() {
    let newState = !this.state.aptBodyVisible;
    this.setState({
      aptBodyVisible: newState
    });
  }

  addItem(tempItem) {
    let tempApts = this.state.myAppointments;
    tempApts.push(tempItem);
    this.setState({
      myAppointments: tempApts
    });
  }

  reOrder(orderBy, orderDir) {
    this.setState({
      orderBy: orderBy,
      orderDir: orderDir
    });
  }

  searchApts(q) {
    this.setState({
      queryText: q
    })
  }

  render() {
    let filteredApts = [];
    let orderBy = this.state.orderBy;
    let orderDir = this.state.orderDir;
    let queryText = this.state.queryText;
    let myAppointments = this.state.myAppointments;

    myAppointments.forEach(item => {
      if(
        (item.petName.toLowerCase().indexOf(queryText) != -1) ||
        (item.ownerName.toLowerCase().indexOf(queryText) != -1) ||
        (item.aptDate.toLowerCase().indexOf(queryText) != -1) ||
        (item.aptNotes.toLowerCase().indexOf(queryText) != -1)
      ) {
        filteredApts.push(item);
      }
    });


    filteredApts = _.orderBy(filteredApts, item => item[orderBy].toLowerCase(), orderDir);

    filteredApts = filteredApts.map((item, index) => {
      return (
        <AptList key = {index}
          singleItem = {item}
          wichItem = {item}
          onDelete = {this.deleteMessage}/>
      )
    })

    return (
      <div className="interface">

      <AddAppointment
        bodyVisible = { this.state.aptBodyVisible }
        handleToggle = { this.toggleAddDisplay }
        addApt = {this.addItem} />
        <SearchAppointments
        orderBy = {this.state.orderBy}
        orderDir = {this.state.orderDir}
        onReOrder = {this.reOrder}
        onSearch = {this.searchApts}/>
        <ul className="item-list media-list">{filteredApts}</ul>
      </div>
    )
  }
}

ReactDOM.render(<MainInterface />, document.getElementById('petAppointments'))
